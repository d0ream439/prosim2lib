#define arrhythmia
#ifdef arrhythmia

#include <iostream>
#include <string>
#include <vector>
#include <tuple>

using namespace std;

#define SV_AFIB1_COARSE     "AFC1\r\n"
#define SV_AFIB1_FINE       "AFF1\r\n"
#define SV_AFIB2_COARSE     "AFC2\r\n"
#define SV_AFIB2_FINE       "AFF2\r\n"
#define SV_ATR_FLUTT        "AFL\r\n"
#define SV_SINUS_ARR        "SINA\r\n"
#define SV_MISSED           "MB80\r\n"
#define SV_ATR_TACH         "ATC\r\n"
#define SV_P_ATR_TACH       "PAT\r\n"
#define SV_NODAL            "NOD\r\n"
#define SV_SVT              "SVT\r\n"

#define PREM_PAC            "PAC\r\n"
#define PREM_PNC            "PNC\r\n"
#define PREM_PVC1_LV        "PVC1S\r\n"
#define PREM_PVC1_LV_EARLY  "PVC1E\r\n"
#define PREM_PVC1_LV_RONT   "PVC1R\r\n"
#define PREM_PVC2_RV        "PVC2S\r\n"
#define PREM_PVC2_RV_EARLY  "PVC2E\r\n"
#define PREM_PVC2_RV_RONT   "PVC2R\r\n"
#define PREM_MULTI_PVCS     "MF\r\n"

#define VENT_PVCS_6P        "PVC6\r\n"
#define VENT_PVCS_12P       "PVC12\r\n"
#define VENT_PVC_24P        "PVC24\r\n"
#define VENT_FREQ_MF        "FMF\r\n"
#define VENT_BIGEMINY       "BIG\r\n"
#define VENT_TRIGEMINY      "TRG\r\n"
#define VENT_PAIR_PVCS      "PAIR\r\n"
#define VENT_RUN_5_PVCS     "RUN5\r\n"
#define VENT_RUN_11_PVCS    "RUN11\r\n"
#define VENT_V_TACH         "VTC\r\n"
#define VENT_VFIB_COARSE    "VFB1\r\n"
#define VENT_VFIB_FINE      "VFB2\r\n"
#define VENT_ASYSTOLE       "ASY\r\n"

#define COND_1ST_DEG_BLK    "1DB\r\n"
#define COND_2ND_DEG_BLK    "2DB\r\n"
#define COND_3RD_DEG_BLK    "3DB\r\n"
#define COND_R_BNDL_BLK     "RBB\r\n"
#define COND_L_BNDL_BLK     "LBB\r\n"

typedef enum :int {
    SUPRAVENTRICULAR    = 0,
    PREMATURE           = 1,
    VENTRICULAR         = 2,
    CONDUCTION_DEFECT   = 3
}enum_arrhythmiaMode_t;

typedef enum:int {
    AFIB1_COARSE    = 0,
    AFIB1_FINE      = 1, 
    AFIB2_COARSE    = 2,
    AFIB2_FINE      = 3,
    ATR_FLUTT       = 4,
    SINUS_ARR       = 5,
    MISSED          = 6,
    ATR_TACH        = 7, 
    P_ATR_TACH      = 8,  
    NODAL           = 9,      
    SVT             = 10               
}enum_supraventricularMode_t;

typedef enum :int {
    PAC             = 0,     
    PNC             = 1,
    PVC1_LV         = 2,  
    PVC1_LV_EARLY   = 3,
    PVC1_LV_RONT    = 4,
    PVC2_RV         = 5,
    PVC2_RV_EARLY   = 6,
    PVC2_RV_RONT    = 7,
    MULTI_PVCS      = 8,
}enum_prematureMode_t;

typedef enum :int {
    PVCS_6P         = 0,  
    PVCS_12P        = 1,
    PVC_24p         = 2,
    FREQ_MF         = 3,
    BIGEMINY        = 4,
    TRIGEMINY       = 5,
    PAIR_PVCS       = 6,
    RUN_5_PVCS      = 7,
    RUN_11_PVCS     = 8,
    V_TACH          = 9,
    VFIB_COARSE     = 10,
    VFIB_FINE       = 11,
    ASYSTOLE        = 12
}enum_ventricularMode_t;

typedef enum :int {

    FIRST_DEG_BLK  =  0,  
    SECOND_DEG_BLK  =  1,
    THIRD_DEG_BLK  =  2,
    R_BNDL_BLK   =  3,
    L_BNDL_BLK   =  4

}enum_conductionDefectMode_t;

typedef struct struct_currentArrhythmiaMode_t
{
    int ARRHYTHMIA_MODE = 0;
    int ARRHYTHMIA_SEMIMODE = 0;
};



class arrhythmia_config {
    public:
        arrhythmia_config();
        struct_currentArrhythmiaMode_t get_currentArrhythmiaMode();
        //void set_currentArrhythmiaMode(int *__mode); should be in prosim2_config
        void set_struct_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_supraventricularMode_t *__semimode);
        void set_struct_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_prematureMode_t *__semimode);
        void set_struct_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_ventricularMode_t *__semimode);
        void set_struct_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode, enum_conductionDefectMode_t *__semimode);

        void create_vector_supraventricularCommand();
        void create_vector_prematureCommand();
        void create_vector_ventricularCommand();
        void create_vector_conductionDefectCommand();

        string get_vector_currentSupraventicularCommand(int *index);
        string get_vector_currentPrematureCommand(int *index);
        string get_vector_currentVentricularCommand(int *index);
        string get_vector_currentConductionDefectCommand(int *index);

        vector<string> convert_configToCommand(struct_currentArrhythmiaMode_t *__struct_currentArrhythmiaMode_t);
        vector<string> get_vector_currentCommand();





    private:
        enum_arrhythmiaMode_t *__enum_arrhythmiaMode_t = NULL ;
        enum_supraventricularMode_t *__enum_supraventricularMode_t = NULL;
        enum_prematureMode_t *__enum_prematureMode_t = NULL;
        enum_ventricularMode_t *__enum_ventricularMode_t = NULL;
        enum_conductionDefectMode_t *__enum_conductionDefectMode_t = NULL;
        struct_currentArrhythmiaMode_t *__struct_currentArrhythmiaMode_t = NULL ;

        vector<string> *__vector_supraventricularCommand;
        vector<string> *__vector_prematureCommand;
        vector<string> *__vector_ventricularCommand;
        vector<string> *__vector_conductionDefectCommand;
        vector<string> *__vector_currentArrhythmiaCommand;

};

#endif