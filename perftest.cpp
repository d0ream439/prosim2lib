#include "perftest.h"

perftest_config::perftest_config()
{
    __struct_performanceWave_config_t = new struct_performanceWave_config_t;
    __struct_R_waveDetection_config_t = new struct_R_waveDetection_config_t;
    __enum_perfTest_mode_t = new enum_perfTest_mode_t;
    __enum_performanceWaves_mode_t = new enum_performanceWaves_mode_t;
    __enum_R_wave_mode_t = new enum_R_wave_mode_t;
    __vector_perfWavesCommand = new vector<string>;
    __vector_perfWavesAmplitudeCommand = new vector<string>;
    __vector_R_waveRateCommand = new vector<string>;
    __vector_R_waveWidthCommand = new vector<string>;
    __vector_R_waveAmplitudeCommand = new vector<string>;
    __vector_currentCommand = new vector<string>;

    set_defaultPerftest_config();
    create_vector_perfWavesCommand();
    create_vector_perfWavesAmplitudeCommand();
    create_vector_R_waveRateCommand();
    create_vector_R_waveWidthCommand();
    create_vector_R_waveAmplitudeCommand();
}


enum_perfTest_mode_t perftest_config::get_currentPerfTestMode()
{
    return *__enum_perfTest_mode_t;
}



enum_performanceWaves_mode_t perftest_config::get_currentPerformanceWavesMode()
{
    return *__enum_performanceWaves_mode_t;
}


enum_R_wave_mode_t perftest_config::get_current_R_wave_mode()
{
    return *__enum_R_wave_mode_t;
}

void perftest_config::set_currentPerfTestMode(enum_perfTest_mode_t *__mode)
{
    *__enum_perfTest_mode_t = *__mode;
}


void perftest_config::set_currentPerformanceWavesMode(enum_performanceWaves_mode_t *__mode)
{
    *__enum_performanceWaves_mode_t = *__mode;
}


void perftest_config::set_current_R_wave_mode(enum_R_wave_mode_t *__mode)
{
    *__enum_R_wave_mode_t = *__mode;
}


struct_performanceWave_config_t perftest_config::get_currentPerformanceWave_config()
{
    return *__struct_performanceWave_config_t;
}


struct_R_waveDetection_config_t perftest_config::get_current_R_waveDetection_config()
{
    return *__struct_R_waveDetection_config_t;
}


int perftest_config::get_PERFORMANCE_WAVES()
{
    return __struct_performanceWave_config_t->waveform;
}


void perftest_config::set_PERFORMANCE_WAVES(int *value)
{
    __struct_performanceWave_config_t->waveform = *value;
}


double perftest_config::get_PERFORMANCE_AMPLITUDE()
{
    return __struct_performanceWave_config_t->amplitude;
}


void perftest_config::set_PERFORMANCE_AMPLITUDE(double *value)
{
    __struct_performanceWave_config_t->amplitude = *value;
}


int perftest_config::get_R_WAVE_RATE()
{
    return __struct_R_waveDetection_config_t->rate;
}


void perftest_config::set_R_WAVE_RATE(int *value)
{
    __struct_R_waveDetection_config_t->rate = *value;
}


int perftest_config::get_R_WAVE_WIDTH()
{
    return __struct_R_waveDetection_config_t->width;
}

void perftest_config::set_R_WAVE_WIDTH(int *value)
{
    __struct_R_waveDetection_config_t->width = *value;
}


double perftest_config::get_R_WAVE_AMPLITUDE()
{
    return __struct_R_waveDetection_config_t->amplitude;
}


void perftest_config::set_R_WAVE_AMPLITUDE(double *value)
{
    __struct_R_waveDetection_config_t->amplitude = *value;
}


void perftest_config::set_defaultPerftest_config()
{

}


void perftest_config::create_vector_perfWavesCommand()
{
    __vector_perfWavesCommand->push_back(PERF_WAVES_SQQUARE_2);   
    __vector_perfWavesCommand->push_back(PERF_WAVES_SQQUARE_0125);
    __vector_perfWavesCommand->push_back(PERF_WAVES_TRI_20);      
    __vector_perfWavesCommand->push_back(PERF_WAVES_TRI_25);      
    __vector_perfWavesCommand->push_back(PERF_WAVES_PULSE_30);    
    __vector_perfWavesCommand->push_back(PERF_WAVES_PULSE_60);    
    __vector_perfWavesCommand->push_back(PERF_WAVES_SIN_05);      
    __vector_perfWavesCommand->push_back(PERF_WAVES_SIN_5);       
    __vector_perfWavesCommand->push_back(PERF_WAVES_SIN_10);      
    __vector_perfWavesCommand->push_back(PERF_WAVES_SIN_40);      
    __vector_perfWavesCommand->push_back(PERF_WAVES_SIN_50);      
    __vector_perfWavesCommand->push_back(PERF_WAVES_SIN_60);      
    __vector_perfWavesCommand->push_back(PERF_WAVES_SIN_100);     
}


void perftest_config::create_vector_perfWavesAmplitudeCommand()
{
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_005);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_010);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_015);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_020);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_025);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_030);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_035);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_040);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_045);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_050);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_100);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_150);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_200);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_250);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_300);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_350);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_400);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_450);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_500);
    __vector_perfWavesAmplitudeCommand->push_back(PERF_WAVES_AMPLITUDE_550);
}


void perftest_config::create_vector_R_waveRateCommand()
{
    __vector_R_waveRateCommand->push_back(R_WAVE_RATE_30);
    __vector_R_waveRateCommand->push_back(R_WAVE_RATE_60);
    __vector_R_waveRateCommand->push_back(R_WAVE_RATE_80);
    __vector_R_waveRateCommand->push_back(R_WAVE_RATE_120);
    __vector_R_waveRateCommand->push_back(R_WAVE_RATE_200);
    __vector_R_waveRateCommand->push_back(R_WAVE_RATE_250);
}


void perftest_config::create_vector_R_waveWidthCommand()
{
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_8);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_10);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_12);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_20);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_30);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_40);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_50);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_60);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_70);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_80);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_90);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_100);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_110);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_120);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_130);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_140);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_150);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_160);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_170);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_180);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_190);
    __vector_R_waveWidthCommand->push_back(R_WAVE_WIDTH_200);
}


void perftest_config::create_vector_R_waveAmplitudeCommand()
{
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_005);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_010);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_015);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_020);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_025);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_030);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_035);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_040);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_045);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_050);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_100);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_150);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_200);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_250);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_300);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_350);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_400);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_450);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_500);
    __vector_R_waveAmplitudeCommand->push_back(R_WAVE_AMPLITUDE_550);
}


string perftest_config::get_vector_currentPerfWavesCommand(int *index)
{
    if(!__vector_perfWavesCommand->empty())
    {
        
        return __vector_perfWavesCommand->at(*index);
    }
}


string perftest_config::get_vector_currentPerfWavesAmplitudeCommand(int *index)
{
    if(!__vector_perfWavesAmplitudeCommand->empty())
    {
        return __vector_perfWavesAmplitudeCommand->at(*index);
    }
}


string perftest_config::get_vector_current_R_waveRateCommand(int *index)
{
    if(!__vector_R_waveRateCommand->empty())
    {
        return __vector_R_waveRateCommand->at(*index);
    }
}


string perftest_config::get_vector_current_R_waveWidthCommand(int *index)
{
    if(!__vector_R_waveWidthCommand->empty())
    {
        return __vector_R_waveWidthCommand->at(*index);
    }
}


string perftest_config::get_vector_current_R_waveAmplitudeCommand(int *index)
{
    if(!__vector_R_waveAmplitudeCommand->empty())
    {
        return __vector_R_waveAmplitudeCommand->at(*index);
    }
}


vector<string> perftest_config::convert_configToCommand(enum_perfTest_mode_t *__mode)
{
    int index_perfWave = 0;
    int index_perfAmplitude = 0;
    int index_R_wave_rate = 0;
    int index_R_wave_width = 0;
    int index_R_wave_amplitude = 0;

    if(get_currentPerfTestMode() ==  enum_perfTest_mode_t::PERFORMANCE_WAVES)
    {
        
        switch (__struct_performanceWave_config_t->waveform)
            {
                case 0:         index_perfWave = 0;          break;
                case 1:         index_perfWave = 1;          break;
                case 2:         index_perfWave = 2;          break;
                case 3:         index_perfWave = 3;          break;
                case 4:         index_perfWave = 4;          break;
                case 5:         index_perfWave = 5;          break;
                case 6:         index_perfWave = 6;          break;
                case 7:         index_perfWave = 7;          break;
                case 8:         index_perfWave = 8;          break;
                case 9:         index_perfWave = 9;          break;
                case 10:        index_perfWave = 10;         break;
                case 11:        index_perfWave = 11;         break;
                case 12:        index_perfWave = 12;         break;

                default:                                    break;
            }
        

        switch((int)((__struct_performanceWave_config_t->amplitude)*100.0))
        {
                case 5:              index_perfAmplitude = 0;         break;
                case 10:             index_perfAmplitude = 1;         break;
                case 15:             index_perfAmplitude = 2;         break;
                case 20:             index_perfAmplitude = 3;         break;
                case 25:             index_perfAmplitude = 4;         break;
                case 30:             index_perfAmplitude = 5;         break;
                case 35:             index_perfAmplitude = 6;         break;
                case 40:             index_perfAmplitude = 7;         break;
                case 45:             index_perfAmplitude = 8;         break;
                case 50:             index_perfAmplitude = 9;         break;
                case 100:            index_perfAmplitude = 10;        break;
                case 150:            index_perfAmplitude = 11;        break;
                case 200:            index_perfAmplitude = 12;        break;
                case 250:            index_perfAmplitude = 13;        break;
                case 300:            index_perfAmplitude = 14;        break;
                case 350:            index_perfAmplitude = 15;        break;
                case 400:            index_perfAmplitude = 16;        break;
                case 450:            index_perfAmplitude = 17;        break;
                case 500:            index_perfAmplitude = 18;        break; 
                case 550:            index_perfAmplitude = 19;        break; 

                default: cout<<" command not found"<<endl;                break;          
        }

        __vector_currentCommand->push_back(get_vector_currentPerfWavesCommand(&index_perfWave));
        __vector_currentCommand->push_back(get_vector_currentPerfWavesAmplitudeCommand(&index_perfAmplitude));
    }

    else if(get_currentPerfTestMode() == enum_perfTest_mode_t::R_WAVE_DETECTION)
    {
        switch(__struct_R_waveDetection_config_t->rate)
        {
                case 30:            index_R_wave_rate = 0;                break;                
                case 60:            index_R_wave_rate = 1;                break;                
                case 80:            index_R_wave_rate = 2;                break;                
                case 120:           index_R_wave_rate = 3;                break;                
                case 200:           index_R_wave_rate = 4;                break;                
                case 250:           index_R_wave_rate = 5;                break;

                default:   cout<<" command not found"<<endl;                   break;                
        }

        switch(__struct_R_waveDetection_config_t->width)
        {
                case 8:                 index_R_wave_width = 0;         break;            
                case 10:                 index_R_wave_width = 1;         break;            
                case 12:                 index_R_wave_width = 2;         break;            
                case 20:                 index_R_wave_width = 3;         break;            
                case 30:                 index_R_wave_width = 4;         break;            
                case 40:                 index_R_wave_width = 5;         break;            
                case 50:                 index_R_wave_width = 6;         break;            
                case 60:                 index_R_wave_width = 7;         break;            
                case 70:                 index_R_wave_width = 8;         break;            
                case 80:                 index_R_wave_width = 9;         break;            
                case 90:                index_R_wave_width = 10;        break;            
                case 100:                index_R_wave_width = 11;        break;            
                case 110:                index_R_wave_width = 12;        break;            
                case 120:                index_R_wave_width = 13;        break;            
                case 130:                index_R_wave_width = 14;        break;            
                case 140:                index_R_wave_width = 15;        break;            
                case 150:                index_R_wave_width = 16;        break;            
                case 160:                index_R_wave_width = 17;        break;            
                case 170:                index_R_wave_width = 18;        break;            
                case 180:                index_R_wave_width = 19;        break;            
                case 190:                index_R_wave_width = 20;        break;            
                case 200:                index_R_wave_width = 21;        break; 
                default:                                                break;               
        }


        switch((convert_amplitudeDoubleToInt(&__struct_R_waveDetection_config_t->amplitude)))
        {
                case 5:              index_R_wave_amplitude = 0;         break;
                case 10:             index_R_wave_amplitude = 1;         break;
                case 15:             index_R_wave_amplitude = 2;         break;
                case 20:             index_R_wave_amplitude = 3;         break;
                case 25:             index_R_wave_amplitude = 4;         break;
                case 30:             index_R_wave_amplitude = 5;         break;
                case 35:             index_R_wave_amplitude = 6;         break;
                case 40:             index_R_wave_amplitude = 7;         break;
                case 45:             index_R_wave_amplitude = 8;         break;
                case 50:             index_R_wave_amplitude = 9;         break;
                case 100:            index_R_wave_amplitude = 10;        break;
                case 150:            index_R_wave_amplitude = 11;        break;
                case 200:            index_R_wave_amplitude = 12;        break;
                case 250:            index_R_wave_amplitude = 13;        break;
                case 300:            index_R_wave_amplitude = 14;        break;
                case 350:            index_R_wave_amplitude = 15;        break;
                case 400:            index_R_wave_amplitude = 16;        break;
                case 450:            index_R_wave_amplitude = 17;        break;
                case 500:            index_R_wave_amplitude = 18;        break; 
                case 550:            index_R_wave_amplitude = 19;        break; 
                
                default:   cout << "ERROR!!" <<endl;              break;          
        }
        
        __vector_currentCommand->push_back(get_vector_current_R_waveRateCommand(&index_R_wave_rate));
        __vector_currentCommand->push_back(get_vector_current_R_waveWidthCommand(&index_R_wave_width));
        __vector_currentCommand->push_back(get_vector_current_R_waveAmplitudeCommand(&index_R_wave_amplitude));

    }

    return *__vector_currentCommand;
}


vector<string> perftest_config::get_vector_currentCommand()
{
    return convert_configToCommand(__enum_perfTest_mode_t);
}


int perftest_config::convert_amplitudeDoubleToInt(double *value)
{
    int temp = (int)((*value)*100.0);
    if(temp > 1     &&      temp <= 5)       { return 5;}
    else if(temp > 5     && temp <= 10)      { return 10;}
    else if(temp > 10    && temp <= 15)      { return 15;}
    else if(temp > 15    && temp <= 20)      { return 20;}
    else if(temp > 20    && temp <= 25)      { return 25;}
    else if(temp > 25    && temp <= 30)      { return 30;}
    else if(temp > 30    && temp <= 35)      { return 35;}
    else if(temp > 35    && temp <= 40)      { return 40;}
    else if(temp > 40    && temp <= 45)      { return 45;}
    else if(temp > 45    && temp <= 50)      { return 50;}
    else if(temp > 90    && temp <= 100)     { return 100;}
    else if(temp > 100   && temp <= 150)     { return 150;}
    else if(temp > 150   && temp <= 200)     { return 200;}
    else if(temp > 200   && temp <= 250)     { return 250;}
    else if(temp > 250   && temp <= 300)     { return 300;}
    else if(temp > 300   && temp <= 350)     { return 250;}
    else if(temp > 350   && temp <= 400)     { return 400;}
    else if(temp > 400   && temp <= 450)     { return 450;}
    else if(temp > 450   && temp <= 500)     { return 500;}
    else if(temp > 500   && temp <= 550)     { return 550;}
}


