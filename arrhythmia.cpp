#include "arrhythmia.h"


arrhythmia_config::arrhythmia_config()
{
    __enum_arrhythmiaMode_t = new enum_arrhythmiaMode_t;
    __enum_supraventricularMode_t = new enum_supraventricularMode_t;
    __enum_prematureMode_t = new enum_prematureMode_t;
    __enum_ventricularMode_t = new enum_ventricularMode_t;
    __enum_conductionDefectMode_t = new enum_conductionDefectMode_t;
    
    __struct_currentArrhythmiaMode_t = new struct_currentArrhythmiaMode_t;
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_MODE = 0;
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_SEMIMODE = 0;

    __vector_supraventricularCommand = new vector<string>;
    __vector_prematureCommand = new vector<string>;
    __vector_ventricularCommand = new vector<string>;
    __vector_conductionDefectCommand = new vector<string>;
    __vector_currentArrhythmiaCommand = new vector<string>;
    
    *__enum_arrhythmiaMode_t = enum_arrhythmiaMode_t::PREMATURE;
    *__enum_supraventricularMode_t = enum_supraventricularMode_t::ATR_TACH;
    *__enum_prematureMode_t = enum_prematureMode_t::PVC2_RV_EARLY;
    *__enum_ventricularMode_t = enum_ventricularMode_t::BIGEMINY;
    *__enum_conductionDefectMode_t = enum_conductionDefectMode_t::THIRD_DEG_BLK;

    create_vector_supraventricularCommand();
    create_vector_prematureCommand();
    create_vector_ventricularCommand();
    create_vector_conductionDefectCommand();

    set_struct_currentArrhythmiaMode(__enum_arrhythmiaMode_t,__enum_prematureMode_t);

}


void  arrhythmia_config::set_struct_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode,enum_supraventricularMode_t *__semimode)
{
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_MODE = static_cast<int>(*__mode);
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_SEMIMODE = static_cast<int>(*__semimode);
}


void  arrhythmia_config::set_struct_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode,enum_prematureMode_t*__semimode)
{
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_MODE = static_cast<int>(*__mode);
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_SEMIMODE =static_cast<int>(*__semimode);
}

void  arrhythmia_config::set_struct_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode,enum_ventricularMode_t *__semimode)
{
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_MODE = static_cast<int>(*__mode);
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_SEMIMODE = static_cast<int>(*__semimode);
}

void  arrhythmia_config::set_struct_currentArrhythmiaMode(enum_arrhythmiaMode_t *__mode,enum_conductionDefectMode_t *__semimode)
{
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_MODE = static_cast<int>(*__mode);
    __struct_currentArrhythmiaMode_t->ARRHYTHMIA_SEMIMODE = static_cast<int>(*__semimode);
}



struct_currentArrhythmiaMode_t arrhythmia_config::get_currentArrhythmiaMode()
{
    return *__struct_currentArrhythmiaMode_t;
}


void arrhythmia_config::create_vector_supraventricularCommand()
{
    __vector_supraventricularCommand->push_back(SV_AFIB1_COARSE);
    __vector_supraventricularCommand->push_back(SV_AFIB1_FINE);
    __vector_supraventricularCommand->push_back(SV_AFIB2_COARSE);
    __vector_supraventricularCommand->push_back(SV_AFIB2_FINE);
    __vector_supraventricularCommand->push_back(SV_ATR_FLUTT);
    __vector_supraventricularCommand->push_back(SV_SINUS_ARR);
    __vector_supraventricularCommand->push_back(SV_MISSED);
    __vector_supraventricularCommand->push_back(SV_ATR_TACH);
    __vector_supraventricularCommand->push_back(SV_P_ATR_TACH);
    __vector_supraventricularCommand->push_back(SV_NODAL);
    __vector_supraventricularCommand->push_back(SV_SVT);
}


void arrhythmia_config::create_vector_prematureCommand()
{
    __vector_prematureCommand->push_back(PREM_PAC);
    __vector_prematureCommand->push_back(PREM_PNC);
    __vector_prematureCommand->push_back(PREM_PVC1_LV);
    __vector_prematureCommand->push_back(PREM_PVC1_LV_EARLY);
    __vector_prematureCommand->push_back(PREM_PVC1_LV_RONT);
    __vector_prematureCommand->push_back(PREM_PVC2_RV);
    __vector_prematureCommand->push_back(PREM_PVC2_RV_EARLY);
    __vector_prematureCommand->push_back(PREM_PVC2_RV_RONT);
    __vector_prematureCommand->push_back(PREM_MULTI_PVCS); 
}


void arrhythmia_config::create_vector_ventricularCommand()
{
    __vector_ventricularCommand->push_back(VENT_PVCS_6P);
    __vector_ventricularCommand->push_back(VENT_PVCS_12P);
    __vector_ventricularCommand->push_back(VENT_PVC_24P);
    __vector_ventricularCommand->push_back(VENT_FREQ_MF);
    __vector_ventricularCommand->push_back(VENT_BIGEMINY);
    __vector_ventricularCommand->push_back(VENT_TRIGEMINY);
    __vector_ventricularCommand->push_back(VENT_PAIR_PVCS);
    __vector_ventricularCommand->push_back(VENT_RUN_5_PVCS);
    __vector_ventricularCommand->push_back(VENT_RUN_11_PVCS);
    __vector_ventricularCommand->push_back(VENT_V_TACH);
    __vector_ventricularCommand->push_back(VENT_VFIB_COARSE);
    __vector_ventricularCommand->push_back(VENT_VFIB_FINE);
    __vector_ventricularCommand->push_back(VENT_ASYSTOLE);
}


void arrhythmia_config::create_vector_conductionDefectCommand()
{
    __vector_conductionDefectCommand->push_back(COND_1ST_DEG_BLK);    
    __vector_conductionDefectCommand->push_back(COND_2ND_DEG_BLK);
    __vector_conductionDefectCommand->push_back(COND_3RD_DEG_BLK);
    __vector_conductionDefectCommand->push_back(COND_R_BNDL_BLK);
    __vector_conductionDefectCommand->push_back(COND_L_BNDL_BLK);
}


string arrhythmia_config::get_vector_currentSupraventicularCommand(int *index)
{
    if(!__vector_supraventricularCommand->empty())
    {
        return __vector_supraventricularCommand->at(*index);
    }
}


string arrhythmia_config::get_vector_currentPrematureCommand(int *index)
{
    if(!__vector_prematureCommand->empty())
    {
        return __vector_prematureCommand->at(*index);
    }
}


string arrhythmia_config::get_vector_currentVentricularCommand(int *index)
{
    if(!__vector_ventricularCommand->empty())
    {
        return __vector_ventricularCommand->at(*index);
    }
}


string arrhythmia_config::get_vector_currentConductionDefectCommand(int *index)
{
    if(!__vector_conductionDefectCommand->empty())
    {
        return __vector_conductionDefectCommand->at(*index);
    }
}

vector<string> arrhythmia_config::convert_configToCommand(struct_currentArrhythmiaMode_t *__struct_currentArrhythmiaMode_t)
{
    switch(__struct_currentArrhythmiaMode_t->ARRHYTHMIA_MODE)
    {
        case 0:
            __vector_currentArrhythmiaCommand->push_back(get_vector_currentSupraventicularCommand(&(__struct_currentArrhythmiaMode_t->ARRHYTHMIA_SEMIMODE)));
            break;
        case 1:
            __vector_currentArrhythmiaCommand->push_back(get_vector_currentPrematureCommand(&(__struct_currentArrhythmiaMode_t->ARRHYTHMIA_SEMIMODE)));
            break;
        case 2:
            __vector_currentArrhythmiaCommand->push_back(get_vector_currentVentricularCommand(&(__struct_currentArrhythmiaMode_t->ARRHYTHMIA_SEMIMODE)));
            break;
        case 3:
            __vector_currentArrhythmiaCommand->push_back(get_vector_currentConductionDefectCommand(&(__struct_currentArrhythmiaMode_t->ARRHYTHMIA_SEMIMODE)));
            break;
        default:
            break;
    }

    return *__vector_currentArrhythmiaCommand;
}


vector<string> arrhythmia_config::get_vector_currentCommand()
{
    return convert_configToCommand(__struct_currentArrhythmiaMode_t);
}