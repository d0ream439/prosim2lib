#define perftest
#ifdef perftest

#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#define PERF_WAVES_SQQUARE_2                    "SQU2\r\n"               
#define PERF_WAVES_SQQUARE_0125                 "SQU.125\r\n"             
#define PERF_WAVES_TRI_20                       "TRI2\r\n"               
#define PERF_WAVES_TRI_25                       "TRI2.5\r\n"
#define PERF_WAVES_PULSE_30                     "PUL30\r\n"
#define PERF_WAVES_PULSE_60                     "PUL60\r\n"
#define PERF_WAVES_SIN_05                       "SIN0.5\r\n"
#define PERF_WAVES_SIN_5                        "SIN5\r\n"
#define PERF_WAVES_SIN_10                       "SIN10\r\n"
#define PERF_WAVES_SIN_40                       "SIN40\r\n"
#define PERF_WAVES_SIN_50                       "SIN50\r\n"
#define PERF_WAVES_SIN_60                       "SIN60\r\n"
#define PERF_WAVES_SIN_100                      "SIN100\r\n"


#define PERF_WAVES_AMPLITUDE_005                "PFA0.05\r\n"                                                    
#define PERF_WAVES_AMPLITUDE_010                "PFA0.10\r\n"
#define PERF_WAVES_AMPLITUDE_015                "PFA0.15\r\n"
#define PERF_WAVES_AMPLITUDE_020                "PFA0.20\r\n"
#define PERF_WAVES_AMPLITUDE_025                "PFA0.25\r\n"
#define PERF_WAVES_AMPLITUDE_030                "PFA0.30\r\n"
#define PERF_WAVES_AMPLITUDE_035                "PFA0.35\r\n"
#define PERF_WAVES_AMPLITUDE_040                "PFA0.40\r\n"
#define PERF_WAVES_AMPLITUDE_045                "PFA0.45\r\n"
#define PERF_WAVES_AMPLITUDE_050                "PFA0.50\r\n"
#define PERF_WAVES_AMPLITUDE_100                "PFA1.00\r\n"
#define PERF_WAVES_AMPLITUDE_150                "PFA1.50\r\n"
#define PERF_WAVES_AMPLITUDE_200                "PFA2.00\r\n"
#define PERF_WAVES_AMPLITUDE_250                "PFA2.50\r\n"
#define PERF_WAVES_AMPLITUDE_300                "PFA3.00\r\n"
#define PERF_WAVES_AMPLITUDE_350                "PFA3.50\r\n"
#define PERF_WAVES_AMPLITUDE_400                "PFA4.00\r\n"
#define PERF_WAVES_AMPLITUDE_450                "PFA4.50\r\n"
#define PERF_WAVES_AMPLITUDE_500                "PFA5.00\r\n"
#define PERF_WAVES_AMPLITUDE_550                "PFA5.50\r\n"


#define R_WAVE_RATE_30                          "RWR30\r\n"
#define R_WAVE_RATE_60                          "RWR60\r\n"
#define R_WAVE_RATE_80                          "RWR80\r\n"
#define R_WAVE_RATE_120                         "RWR120\r\n"
#define R_WAVE_RATE_200                         "RWR200\r\n"
#define R_WAVE_RATE_250                         "RWR250\r\n"


#define R_WAVE_WIDTH_8                          "RWW8\r\n"
#define R_WAVE_WIDTH_10                         "RWW10\r\n"
#define R_WAVE_WIDTH_12                         "RWW12\r\n"
#define R_WAVE_WIDTH_20                         "RWW20\r\n"
#define R_WAVE_WIDTH_30                         "RWW30\r\n"
#define R_WAVE_WIDTH_40                         "RWW40\r\n"
#define R_WAVE_WIDTH_50                         "RWW50\r\n"
#define R_WAVE_WIDTH_60                         "RWW60\r\n"
#define R_WAVE_WIDTH_70                         "RWW70\r\n"
#define R_WAVE_WIDTH_80                         "RWW80\r\n"
#define R_WAVE_WIDTH_90                         "RWW90\r\n"
#define R_WAVE_WIDTH_100                        "RWW100\r\n"
#define R_WAVE_WIDTH_110                        "RWW110\r\n"
#define R_WAVE_WIDTH_120                        "RWW120\r\n"
#define R_WAVE_WIDTH_130                        "RWW130\r\n"
#define R_WAVE_WIDTH_140                        "RWW140\r\n"
#define R_WAVE_WIDTH_150                        "RWW150\r\n"
#define R_WAVE_WIDTH_160                        "RWW160\r\n"
#define R_WAVE_WIDTH_170                        "RWW170\r\n"
#define R_WAVE_WIDTH_180                        "RWW180\r\n"
#define R_WAVE_WIDTH_190                        "RWW190\r\n"
#define R_WAVE_WIDTH_200                        "RWW200\r\n"


#define R_WAVE_AMPLITUDE_005                   "RWA0.05\r\n"
#define R_WAVE_AMPLITUDE_010                   "RWA0.10\r\n"
#define R_WAVE_AMPLITUDE_015                   "RWA0.15\r\n"
#define R_WAVE_AMPLITUDE_020                   "RWA0.20\r\n"
#define R_WAVE_AMPLITUDE_025                   "RWA0.25\r\n"
#define R_WAVE_AMPLITUDE_030                   "RWA0.30\r\n"
#define R_WAVE_AMPLITUDE_035                   "RWA0.35\r\n"
#define R_WAVE_AMPLITUDE_040                   "RWA0.40\r\n"
#define R_WAVE_AMPLITUDE_045                   "RWA0.45\r\n"
#define R_WAVE_AMPLITUDE_050                   "RWA0.50\r\n"
#define R_WAVE_AMPLITUDE_100                   "RWA1.00\r\n"
#define R_WAVE_AMPLITUDE_150                   "RWA1.50\r\n"
#define R_WAVE_AMPLITUDE_200                   "RWA2.00\r\n"
#define R_WAVE_AMPLITUDE_250                   "RWA2.50\r\n"
#define R_WAVE_AMPLITUDE_300                   "RWA3.00\r\n"
#define R_WAVE_AMPLITUDE_350                   "RWA3.50\r\n"
#define R_WAVE_AMPLITUDE_400                   "RWA4.00\r\n"
#define R_WAVE_AMPLITUDE_450                   "RWA4.50\r\n"
#define R_WAVE_AMPLITUDE_500                   "RWA5.00\r\n"
#define R_WAVE_AMPLITUDE_550                   "RWA5.50\r\n"


using namespace std;


typedef enum :int{
    PERFORMANCE_WAVES = 0,
    R_WAVE_DETECTION = 1
}enum_perfTest_mode_t;

typedef enum :int{
    PERFORMANCE_WAVEFORM = 0,
    PERFORMANCE_WAVES_AMPLITUDE = 1
}enum_performanceWaves_mode_t;

typedef enum :int{
    R_WAVE_RATE = 0,
    R_WAVE_WIDTH = 1,
    R_WAVE_AMPLITUDE = 2
}enum_R_wave_mode_t;


typedef struct{
    int waveform = 0;
    double amplitude = 0.00;
}struct_performanceWave_config_t;


typedef struct{
    int rate = 0;
    int width = 0;
    double amplitude = 0;
}struct_R_waveDetection_config_t;



  


class perftest_config{
    public:
        perftest_config();
        enum_perfTest_mode_t get_currentPerfTestMode();
        enum_performanceWaves_mode_t get_currentPerformanceWavesMode();
        enum_R_wave_mode_t get_current_R_wave_mode();
        void set_currentPerfTestMode(enum_perfTest_mode_t *__mode);
        void set_currentPerformanceWavesMode(enum_performanceWaves_mode_t *__mode);
        void set_current_R_wave_mode(enum_R_wave_mode_t *__mode);

        struct_performanceWave_config_t get_currentPerformanceWave_config();
        struct_R_waveDetection_config_t get_current_R_waveDetection_config();

        int get_PERFORMANCE_WAVES();
        void set_PERFORMANCE_WAVES(int *value);
        double get_PERFORMANCE_AMPLITUDE();
        void set_PERFORMANCE_AMPLITUDE(double *value);
        int get_R_WAVE_RATE();
        void set_R_WAVE_RATE(int *value);
        int get_R_WAVE_WIDTH();
        void set_R_WAVE_WIDTH(int *value);
        double get_R_WAVE_AMPLITUDE();
        void set_R_WAVE_AMPLITUDE(double *value);

        void set_defaultPerftest_config();
        void create_vector_perfWavesCommand();
        void create_vector_perfWavesAmplitudeCommand();
        void create_vector_R_waveRateCommand();
        void create_vector_R_waveWidthCommand();
        void create_vector_R_waveAmplitudeCommand();
        string get_vector_currentPerfWavesCommand(int *index);
        string get_vector_currentPerfWavesAmplitudeCommand(int *index);
        string get_vector_current_R_waveRateCommand(int *index);
        string get_vector_current_R_waveWidthCommand(int *index);
        string get_vector_current_R_waveAmplitudeCommand(int *index);
        vector<string> convert_configToCommand(enum_perfTest_mode_t *__mode);
        vector<string> get_vector_currentCommand();

        int convert_amplitudeDoubleToInt(double *value);

    private:
        enum_perfTest_mode_t *__enum_perfTest_mode_t = NULL ;
        enum_performanceWaves_mode_t *__enum_performanceWaves_mode_t = NULL;
        enum_R_wave_mode_t *__enum_R_wave_mode_t = NULL ;
        struct_performanceWave_config_t *__struct_performanceWave_config_t = NULL;
        struct_R_waveDetection_config_t *__struct_R_waveDetection_config_t = NULL;
        vector<string> *__vector_perfWavesCommand = NULL ;
        vector<string> *__vector_perfWavesAmplitudeCommand = NULL ;
        vector<string> *__vector_R_waveRateCommand = NULL ;
        vector<string> *__vector_R_waveWidthCommand = NULL ;
        vector<string> *__vector_R_waveAmplitudeCommand = NULL ;
        vector<string> *__vector_currentCommand = NULL;
};
#endif