#include <iostream>
#include "prosim2_config.h"
#include <string>


using namespace std;
/*
int main()
{
    //string command;
    int value;
    double value_f1 , value_f2;
    bool value_b;
    enum_prosim2Mode_t mode = enum_prosim2Mode_t::ECGFUNC;
    int a =0;
    enum_artifact_t artifact = enum_artifact_t::OFF;
    double b =  2.0256;
    vector<string> command;
    Prosim2_config  *_prosim2_config = new Prosim2_config;
    struct_ECGFuncConfig_t t =_prosim2_config->get_currentECGFunction_config(); ;
    enum_ECGFunc_mode mode1;

    mode = enum_prosim2Mode_t::ARRYTHMIA;

    enum_arrhythmiaMode_t Amode =enum_arrhythmiaMode_t::CONDUCTION_DEFECT;

    //enum_supraventricularMode_t semimode = enum_supraventricularMode_t::ATR_TACH;
    //enum_prematureMode_t semimode = enum_prematureMode_t::PNC;
    //enum_ventricularMode_t semimode = enum_ventricularMode_t::BIGEMINY;
    enum_conductionDefectMode_t semimode = enum_conductionDefectMode_t::SECOND_DEG_BLK;
    arrhythmia_config arrhy;
    
    _prosim2_config->set_currentArrhythmiaMode(&Amode, &semimode);

    command = _prosim2_config->get_currentCommand(&mode);

    cout <<"command: " << command.at(0) << endl;


/*
    cout << "bpm: " <<t.BPM << endl;
    cout << "pttype: " << t.PT_TYPE <<endl <<flush;
    cout << "amplitude: " <<t.AMPLITUDE <<endl;
    //cout << b <<endl;


    mode = _prosim2_config->get_currentMode();
    cout <<"current mode: " << mode <<endl;
    _prosim2_config->set_currentMode(&mode);
    mode = _prosim2_config->get_currentMode();
    cout <<"current mode: " << mode <<endl;
*/
/*
    mode1 = enum_ECGFunc_mode::BPM;
    cout << " BPM Enter value :";
    cin >> value;
    _prosim2_config->set_currentECGFunction_config(&mode1,&value);

    mode1 = enum_ECGFunc_mode::AMPLITUDE;
    cout << " AMPLITUDE Enter value :";
    cin >> value_f1;
    _prosim2_config->set_currentECGFunction_config(&mode1,&value_f1);

    mode1 = enum_ECGFunc_mode::PT_TYPE;
    cout << "PT_TYPE Enter value :";
    cin >> value_b;
    _prosim2_config->set_currentECGFunction_config(&mode1,&value_b);

    mode1 = enum_ECGFunc_mode::ST;
    cout << "ST Enter value :";
    cin >> value_f2;
    _prosim2_config->set_currentECGFunction_config(&mode1,&value_f2);

    
    mode1 = enum_ECGFunc_mode::ARTIFACT_SIM;
    cout << "ARTIFACT_SIM Enter value :" ;
    cin >> a;
    artifact = static_cast<enum_artifact_t>(a); //
    _prosim2_config->set_currentECGFunction_config(&mode1,&artifact);
    
    cout << a <<endl;

    t = _prosim2_config->get_currentECGFunction_config();
    
    cout << "bpm: " <<t.BPM << endl;
    cout << "pttype: " << t.PT_TYPE <<endl;
    cout << "amplitude: " <<t.AMPLITUDE <<endl;
    cout << "ST: " <<t.ST <<endl;
    cout << "ARTIFACT: " <<t.ARTIFACT <<endl;
    
   command = _prosim2_config->get_currentCommand(&mode);
   cout << "Command 0 :" << command.at(0) << endl; 
   cout << "Command 1 :" << command.at(1) << endl;
   cout << "Command 2 :" << command.at(2) << endl;
   cout << "Command 3 :" << command.at(3) << endl;
   cout << "Command 4 :" << command.at(4) << endl;
   
   cout << value <<endl;
   cout << value_b <<endl;
   cout << value_f1 <<endl;
   cout <<value_f2 <<endl;
   

   //amplitude,st incorrect!!!!!!!!!!!!


    //cout << "b:" << b <<endl;

}*/

int main()
{
    vector<string> command ;
    enum_prosim2Mode_t _enum_prosim2Mode_t = enum_prosim2Mode_t::PERFTEST;
    enum_prosim2Mode_t currentMode;
    enum_perfTest_mode_t a = enum_perfTest_mode_t::R_WAVE_DETECTION;
    enum_R_wave_mode_t Rmode = enum_R_wave_mode_t::R_WAVE_RATE;
    enum_performanceWaves_mode_t Pfmode = enum_performanceWaves_mode_t::PERFORMANCE_WAVEFORM;
    struct_performanceWave_config_t perfconfig;
    struct_R_waveDetection_config_t Rconfig;
    Prosim2_config *_prosim2_config = new Prosim2_config;
    currentMode = _prosim2_config->get_currentMode();
    _prosim2_config->set_currentMode(&_enum_prosim2Mode_t);
    perfconfig = _prosim2_config->get_currentPerformanceWave_config(&a);
    //cout <<perfconfig.waveform<<endl;
    //cout <<perfconfig.amplitude<<endl;
    /*
    a = enum_perfTest_mode_t::PERFORMANCE_WAVEFORM;
    int wave = 3;
    _prosim2_config->set_currentPerfTest_config(&a , &wave );
    a = enum_perfTest_mode_t::PERFORMANCE_WAVES_AMPLITUDE;
    double amp = 0.05;
    _prosim2_config->set_currentPerfTest_config(&a,&amp);
    */
   /*
    a = enum_perfTest_mode_t::R_WAVE_RATE;
    int wave = 30;
    _prosim2_config->set_currentPerfTest_config(&a , &wave );
    a = enum_perfTest_mode_t::R_WAVE_WIDTH;
    wave = 130;
    _prosim2_config->set_currentPerfTest_config(&a , &wave );
    a = enum_perfTest_mode_t::R_WAVE_AMPLITUDE;
    double amplitude = 0.35;
    _prosim2_config->set_currentPerfTest_config(&a , &amplitude );
    
    a = enum_perfTest_mode_t::R_WAVE_DETECTION;
    Rconfig = _prosim2_config->get_current_R_wavDetection_config(&a);
    cout <<Rconfig.rate<<endl;
    cout <<Rconfig.width<<endl;
    cout <<Rconfig.amplitude<<endl;
    command = _prosim2_config->get_currentCommand(&_enum_prosim2Mode_t);
    cout <<command.at(0)<<endl;
    cout <<command.at(1)<<endl;
    cout <<command.at(2)<<endl;
    Rconfig = _prosim2_config->get_current_R_wavDetection_config(&a);
    a = enum_perfTest_mode_t::R_WAVE_AMPLITUDE;
    */
   int rate = 120;
   int width = 8;
   double amplitude = 0.05;
   _prosim2_config->set_currentPerfTest_config(&a,&Rmode,&rate);
   Rmode = enum_R_wave_mode_t::R_WAVE_WIDTH;
   _prosim2_config->set_currentPerfTest_config(&a,&Rmode,&width);
   Rmode = enum_R_wave_mode_t::R_WAVE_AMPLITUDE;
   _prosim2_config->set_currentPerfTest_config(&a,&Rmode,&amplitude);
   Rconfig = _prosim2_config->get_current_R_waveDetection_config(&a);

    cout << "******" << endl;
   cout << Rconfig.rate << endl;
   cout << Rconfig.width << endl;
   cout << Rconfig.amplitude << endl;

   command = _prosim2_config->get_currentCommand(&_enum_prosim2Mode_t);

   cout << command.at(0) << endl;
   cout << command.at(1) << endl;
   cout << command.at(2) << endl;
   

   





}