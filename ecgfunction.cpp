#include "ecgfunction.h"

ECGFunction_config::ECGFunction_config()
{
    __struct_ECGFuncConfig_t = new struct_ECGFuncConfig_t;
    __vector_ECGFuncConfigCommand = new vector<string>;
    __vector_BPMCommand = new vector<string>;
    __vector_AMPLITUDECommand = new vector<string>;
    __vector_PT_TYPECommand = new vector<string>;
    __vector_STCommand = new vector<string>;
    __vector_ARTIFACT_SIMCommand = new vector<string>;

    set_defaultECGFunction_config();
    create_vector_BPMCommand();
    create_vector_AMPLITUDECommand();
    create_vector_PT_TYPECommand();
    create_vector_STCommand();
    create_vector_ARTIFACT_SIMCommand();
}


struct_ECGFuncConfig_t ECGFunction_config::get_currentECGFunction_config()
{

    return *__struct_ECGFuncConfig_t;
}


int ECGFunction_config::get_BPM()
{
    return __struct_ECGFuncConfig_t->BPM;
}


void ECGFunction_config::set_BPM(int *value)
{
    __struct_ECGFuncConfig_t->BPM = *value;
}



double ECGFunction_config::get_AMPLITUDE()
{
    return __struct_ECGFuncConfig_t->AMPLITUDE;
}


void ECGFunction_config::set_AMPLITUDE(double *value)
{
    __struct_ECGFuncConfig_t->AMPLITUDE = *value;
}


bool ECGFunction_config::get_PT_TYPE()
{
    return __struct_ECGFuncConfig_t->PT_TYPE;
}


void ECGFunction_config::set_PT_TYPE(bool *value)
{
    __struct_ECGFuncConfig_t->PT_TYPE = *value;
}

double ECGFunction_config::get_ST()
{
    return __struct_ECGFuncConfig_t->ST;
}


void ECGFunction_config::set_ST(double *value)
{
    __struct_ECGFuncConfig_t->ST = *value;
}


enum_artifact_t ECGFunction_config::get_ARTIFACT_SIM()
{
    return __struct_ECGFuncConfig_t->ARTIFACT;
}


void ECGFunction_config::set_ARTIFACT_SIM(enum_artifact_t *value)
{
    __struct_ECGFuncConfig_t->ARTIFACT = *value;
}


void ECGFunction_config::set_defaultECGFunction_config()
{
    set_BPM(&__default_BPM);
    set_AMPLITUDE(&__default_AMPLITUDE);
    set_PT_TYPE(&__default_PT_TYPE);
    set_ST(&__default_ST);
    set_ARTIFACT_SIM(&__default_ARTIFACT_SIM);
}

void ECGFunction_config::create_vector_BPMCommand()
{
    __vector_BPMCommand->push_back(NORMALSINUS_30_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_40_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_45_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_60_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_80_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_90_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_100_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_120_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_140_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_160_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_180_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_200_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_220_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_240_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_260_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_280_BPM);
    __vector_BPMCommand->push_back(NORMALSINUS_300_BPM);   
}


void ECGFunction_config::create_vector_AMPLITUDECommand()
{
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_005);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_010);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_015);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_020);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_025);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_030);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_035);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_040);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_045);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_050);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_100);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_150);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_200);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_250);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_300);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_350);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_400);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_450);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_500);
    __vector_AMPLITUDECommand->push_back(AMPLITUDE_550);
}


void ECGFunction_config::create_vector_PT_TYPECommand()
{
    __vector_PT_TYPECommand->push_back(PT_TYPE_PEDIATRIC);
    __vector_PT_TYPECommand->push_back(PT_TYPE_ADULT);
}


void ECGFunction_config::create_vector_STCommand()
{
    __vector_STCommand->push_back(ST_ELEVATION_M080);
    __vector_STCommand->push_back(ST_ELEVATION_M070);
    __vector_STCommand->push_back(ST_ELEVATION_M060);
    __vector_STCommand->push_back(ST_ELEVATION_M050);
    __vector_STCommand->push_back(ST_ELEVATION_M040);
    __vector_STCommand->push_back(ST_ELEVATION_M030);
    __vector_STCommand->push_back(ST_ELEVATION_M020);
    __vector_STCommand->push_back(ST_ELEVATION_M010);
    __vector_STCommand->push_back(ST_ELEVATION_M005);
    __vector_STCommand->push_back(ST_ELEVATION_Z000);
    __vector_STCommand->push_back(ST_ELEVATION_P005);
    __vector_STCommand->push_back(ST_ELEVATION_P010);
    __vector_STCommand->push_back(ST_ELEVATION_P020);
    __vector_STCommand->push_back(ST_ELEVATION_P030);
    __vector_STCommand->push_back(ST_ELEVATION_P040);
    __vector_STCommand->push_back(ST_ELEVATION_P050);
    __vector_STCommand->push_back(ST_ELEVATION_P060);
    __vector_STCommand->push_back(ST_ELEVATION_P070);
    __vector_STCommand->push_back(ST_ELEVATION_P080);
}


void ECGFunction_config::create_vector_ARTIFACT_SIMCommand()
{
    __vector_ARTIFACT_SIMCommand->push_back(ARTIFACT_SIM_OFF);
    __vector_ARTIFACT_SIMCommand->push_back(ARTIFACT_SIM_50HZ);
    __vector_ARTIFACT_SIMCommand->push_back(ARTIFACT_SIM_60HZ);
    __vector_ARTIFACT_SIMCommand->push_back(ARTIFACT_SIM_MUSCLE);
    __vector_ARTIFACT_SIMCommand->push_back(ARTIFACT_SIM_WANDERING);
    __vector_ARTIFACT_SIMCommand->push_back(ARTIFACT_SIM_RESPIRATION);
}



string ECGFunction_config::get_vector_currentBPMCommand(int *index)
{
    if(!__vector_BPMCommand->empty())
    {
        return __vector_BPMCommand->at(*index);
    }  
}


string ECGFunction_config::get_vector_currentAMPLITUDE(int *index)
{
    if(!__vector_AMPLITUDECommand->empty())
    {
        return __vector_AMPLITUDECommand->at(*index);
    }
}


string ECGFunction_config::get_vector_currentPT_TYPECommand(int *index)
{
    if(!__vector_PT_TYPECommand->empty())
    {
        return __vector_PT_TYPECommand->at(*index);
    }
}


string ECGFunction_config::get_vector_currentSTCommand(int *index)
{
    if(!__vector_STCommand->empty())
    {
        return __vector_STCommand->at(*index);
    }
}

string ECGFunction_config::get_vector_currentARTIFACT_SIMCommand(int *index)
{
    if(!__vector_ARTIFACT_SIMCommand->empty())
    {
        return __vector_ARTIFACT_SIMCommand->at(*index);
    }
}


vector<string> ECGFunction_config::convert_configToCommand(struct_ECGFuncConfig_t *__struct_ECGFuncConfig_t)
{
    int index_BPM =0;
    int index_AMPLITUDE = 0;
    int index_PT_TYPE = 0;
    int index_ST = 0;
    int index_ARTIFACT_SIM = 0;

    switch(__struct_ECGFuncConfig_t->BPM)
    {
        case 30:    index_BPM = 0;     break;
        case 40:    index_BPM = 1;     break;
        case 45:    index_BPM = 2;     break;
        case 60:    index_BPM = 3;     break;
        case 80:    index_BPM = 4;     break;
        case 90:    index_BPM = 5;     break;
        case 100:   index_BPM = 6;     break;
        case 120:   index_BPM = 7;     break;
        case 140:   index_BPM = 8;     break;
        case 160:   index_BPM = 9;     break;
        case 180:   index_BPM = 10;    break;
        case 200:   index_BPM = 11;    break;
        case 220:   index_BPM = 12;    break;
        case 240:   index_BPM = 13;    break;
        case 260:   index_BPM = 14;    break;
        case 280:   index_BPM = 15;    break;
        case 300:   index_BPM = 16;    break;
        
        default: cout << "invalid value" <<endl;    break;         
    }


    switch ((int)((__struct_ECGFuncConfig_t->AMPLITUDE)*100))
    {
        case 5:     index_AMPLITUDE = 0;        break;
        case 10:    index_AMPLITUDE = 1;        break;
        case 15:    index_AMPLITUDE = 2;        break;
        case 20:    index_AMPLITUDE = 3;        break;
        case 25:    index_AMPLITUDE = 4;        break;
        case 30:    index_AMPLITUDE = 5;        break;
        case 35:    index_AMPLITUDE = 6;        break;
        case 40:    index_AMPLITUDE = 7;        break;
        case 45:    index_AMPLITUDE = 8;        break;
        case 50:    index_AMPLITUDE = 9;        break;
        case 100:   index_AMPLITUDE = 10;       break;
        case 150:   index_AMPLITUDE = 11;       break;
        case 200:   index_AMPLITUDE = 12;       break;
        case 250:   index_AMPLITUDE = 13;       break;
        case 300:   index_AMPLITUDE = 14;       break;
        case 350:   index_AMPLITUDE = 15;       break;
        case 400:   index_AMPLITUDE = 16;       break;
        case 450:   index_AMPLITUDE = 17;       break;
        case 500:   index_AMPLITUDE = 18;       break;
        case 550:   index_AMPLITUDE = 19;       break;

        default: cout << "invalid value" <<endl;    break;
    }


    switch(__struct_ECGFuncConfig_t->PT_TYPE)
    {
        case 0: index_PT_TYPE = 0;  break;
        case 1: index_PT_TYPE = 1;  break;

        default: cout << "invalid value" <<endl;    break;
    }


    switch((int)((__struct_ECGFuncConfig_t->ST)*100))
    {
        case -80:      index_ST = 0;        break;
        case -70:      index_ST = 1;        break;
        case -60:      index_ST = 2;        break;
        case -50:      index_ST = 3;        break;
        case -40:      index_ST = 4;        break;
        case -30:      index_ST = 5;        break;
        case -20:      index_ST = 6;        break;
        case -10:      index_ST = 7;        break;
        case -5:       index_ST = 8;        break;
        case  0:       index_ST = 9;        break;
        case  5:       index_ST = 10;       break;
        case  10:      index_ST = 11;       break;
        case  20:      index_ST = 12;       break;
        case  30:      index_ST = 13;       break;
        case  40:      index_ST = 14;       break;
        case  50:      index_ST = 15;       break;
        case  60:      index_ST = 16;       break;
        case  70:      index_ST = 17;       break;
        case  80:      index_ST = 18;       break;   

        default: cout << "invalid value" <<endl;    break;
    }
    

    switch (__struct_ECGFuncConfig_t->ARTIFACT)
    {
        case OFF:               index_ARTIFACT_SIM = 0;     break;
        case NOISE_50HZ:        index_ARTIFACT_SIM = 1;     break;
        case NOISE_60HZ:        index_ARTIFACT_SIM = 2;     break;
        case MUSCLE:            index_ARTIFACT_SIM = 3;     break;
        case WANDERING:         index_ARTIFACT_SIM = 4;     break;
        case RESPIRATION:       index_ARTIFACT_SIM = 5;     break;

        default: cout << "invalid value" <<endl;            break;
    }

    __vector_ECGFuncConfigCommand->push_back(get_vector_currentBPMCommand(&index_BPM));
    __vector_ECGFuncConfigCommand->push_back(get_vector_currentAMPLITUDE(&index_AMPLITUDE));
    __vector_ECGFuncConfigCommand->push_back(get_vector_currentPT_TYPECommand(&index_PT_TYPE));
    __vector_ECGFuncConfigCommand->push_back(get_vector_currentSTCommand(&index_ST));
    __vector_ECGFuncConfigCommand->push_back(get_vector_currentARTIFACT_SIMCommand(&index_ARTIFACT_SIM));

    return *__vector_ECGFuncConfigCommand;  
}


vector<string> ECGFunction_config::get_vector_currentCommand()
{
    return convert_configToCommand(__struct_ECGFuncConfig_t);
}


