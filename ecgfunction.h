#define ecgfunction
#ifdef ecgfunction 

#include <iostream>
#include <vector>
#include <string>

#define NORMALSINUS_DEFAULT_BPM  80
#define NORMALSINUS_DEFAULT_AMPLITUDE  1.0
#define NORMALSINUS_DEFAULT_PT_TYPE   true
#define NORMALSINUS_DEFAULT_ST   0.0
#define NORMALSINUS_DEFAULT_ARTIFACT_SIM OFF

#define NORMALSINUS_30_BPM      "NSR30\r\n"
#define NORMALSINUS_40_BPM      "NSR40\r\n"
#define NORMALSINUS_45_BPM      "NSR45\r\n"
#define NORMALSINUS_60_BPM      "NSR60\r\n"
#define NORMALSINUS_80_BPM      "NSR80\r\n"
#define NORMALSINUS_90_BPM      "NSR90\r\n"
#define NORMALSINUS_100_BPM     "NSR100\r\n"
#define NORMALSINUS_120_BPM     "NSR120\r\n"
#define NORMALSINUS_140_BPM     "NSR140\r\n"
#define NORMALSINUS_160_BPM     "NSR160\r\n"
#define NORMALSINUS_180_BPM     "NSR180\r\n"
#define NORMALSINUS_200_BPM     "NSR200\r\n"
#define NORMALSINUS_220_BPM     "NSR220\r\n"
#define NORMALSINUS_240_BPM     "NSR240\r\n"
#define NORMALSINUS_260_BPM     "NSR260\r\n"
#define NORMALSINUS_280_BPM     "NSR280\r\n"
#define NORMALSINUS_300_BPM     "NSR300\r\n"


#define AMPLITUDE_005         "NSA0.05\r\n"
#define AMPLITUDE_010         "NSA0.10\r\n"
#define AMPLITUDE_015         "NSA0.15\r\n"
#define AMPLITUDE_020         "NSA0.20\r\n"
#define AMPLITUDE_025         "NSA0.25\r\n"
#define AMPLITUDE_030         "NSA0.30\r\n"
#define AMPLITUDE_035         "NSA0.35\r\n"
#define AMPLITUDE_040         "NSA0.40\r\n"
#define AMPLITUDE_045         "NSA0.45\r\n"
#define AMPLITUDE_050         "NSA0.50\r\n"
#define AMPLITUDE_100         "NSA1.00\r\n"
#define AMPLITUDE_150         "NSA1.50\r\n"
#define AMPLITUDE_200         "NSA2.00\r\n"
#define AMPLITUDE_250         "NSA2.50\r\n"
#define AMPLITUDE_300         "NSA3.00\r\n"
#define AMPLITUDE_350         "NSA3.50\r\n"
#define AMPLITUDE_400         "NSA4.00\r\n"
#define AMPLITUDE_450         "NSA4.50\r\n"
#define AMPLITUDE_500         "NSA5.00\r\n"
#define AMPLITUDE_550         "NSA5.50\r\n"

#define PT_TYPE_ADULT       "ADULT\r\n"
#define PT_TYPE_PEDIATRIC   "PEDS\r\n"


#define ST_ELEVATION_M080       "STD-0.8\r\n"
#define ST_ELEVATION_M070       "STD-0.7\r\n"
#define ST_ELEVATION_M060       "STD-0.6\r\n"
#define ST_ELEVATION_M050       "STD-0.5\r\n"
#define ST_ELEVATION_M040       "STD-0.4\r\n"
#define ST_ELEVATION_M030       "STD-0.3\r\n"
#define ST_ELEVATION_M020       "STD-0.2\r\n"
#define ST_ELEVATION_M010       "STD-0.1\r\n"
#define ST_ELEVATION_M005       "STD-0.05\r\n"
#define ST_ELEVATION_Z000       "STD0\r\n"
#define ST_ELEVATION_P005       "STD+0.05\r\n"
#define ST_ELEVATION_P010       "STD+0.1\r\n"
#define ST_ELEVATION_P020       "STD+0.2\r\n"
#define ST_ELEVATION_P030       "STD+0.3\r\n"
#define ST_ELEVATION_P040       "STD+0.4\r\n"
#define ST_ELEVATION_P050       "STD+0.5\r\n"
#define ST_ELEVATION_P060       "STD+0.6\r\n"
#define ST_ELEVATION_P070       "STD+0.7\r\n"
#define ST_ELEVATION_P080       "STD+0.8\r\n"

#define ARTIFACT_SIM_OFF            "EAOFF\r\n"
#define ARTIFACT_SIM_50HZ           "EA50\r\n"
#define ARTIFACT_SIM_60HZ           "EA60\r\n"
#define ARTIFACT_SIM_MUSCLE         "EAMSC\r\n"
#define ARTIFACT_SIM_WANDERING      "EAWNDR\r\n"
#define ARTIFACT_SIM_RESPIRATION    "EARESP\r\n"


using namespace std;


//Uppercase in enum;
typedef enum : int {
    OFF = 0,
    NOISE_50HZ = 1,
    NOISE_60HZ = 2,
    MUSCLE = 3,
    WANDERING = 4,
    RESPIRATION = 5
    
}enum_artifact_t;

typedef enum :int {
    BPM = 0,
    AMPLITUDE = 1,
    PT_TYPE = 2,
    ST = 3,
    ARTIFACT_SIM = 4
}enum_ECGFunc_mode;

typedef struct struct_ECGFuncConfig_t{
    int BPM ;
    double AMPLITUDE ;
    bool PT_TYPE; //true = adult ,false = pediatric;
    double ST;
    enum_artifact_t ARTIFACT;
};

class ECGFunction_config{
    public:
        ECGFunction_config();
        struct_ECGFuncConfig_t get_currentECGFunction_config();

        int get_BPM();
        void set_BPM(int *value);
        double get_AMPLITUDE();
        void set_AMPLITUDE(double *value);
        bool get_PT_TYPE();
        void set_PT_TYPE(bool *value);
        double get_ST();
        void set_ST(double *value);
        enum_artifact_t get_ARTIFACT_SIM();
        void set_ARTIFACT_SIM(enum_artifact_t *value);

        void set_defaultECGFunction_config();

        void create_vector_BPMCommand();
        void create_vector_AMPLITUDECommand();
        void create_vector_PT_TYPECommand();
        void create_vector_STCommand();
        void create_vector_ARTIFACT_SIMCommand();
        string get_vector_currentBPMCommand(int *index);
        string get_vector_currentAMPLITUDE(int *index);
        string get_vector_currentPT_TYPECommand(int *index);
        string get_vector_currentSTCommand(int *index);
        string get_vector_currentARTIFACT_SIMCommand(int *index);
        vector<string> convert_configToCommand(struct_ECGFuncConfig_t *__struct_ECGFuncConfig_t);
        vector<string> get_vector_currentCommand();
        


        
    private:

        struct_ECGFuncConfig_t *__struct_ECGFuncConfig_t;
        vector<string> *__vector_BPMCommand;
        vector<string> *__vector_AMPLITUDECommand;
        vector<string> *__vector_PT_TYPECommand;
        vector<string> *__vector_STCommand;
        vector<string> *__vector_ARTIFACT_SIMCommand;
        vector<string> *__vector_ECGFuncConfigCommand;

        int __default_BPM = NORMALSINUS_DEFAULT_BPM;
        double __default_AMPLITUDE = NORMALSINUS_DEFAULT_AMPLITUDE;
        bool __default_PT_TYPE = NORMALSINUS_DEFAULT_PT_TYPE;
        double __default_ST = NORMALSINUS_DEFAULT_ST;
        enum_artifact_t __default_ARTIFACT_SIM = NORMALSINUS_DEFAULT_ARTIFACT_SIM;


};





#endif